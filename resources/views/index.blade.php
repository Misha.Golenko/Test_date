@extends('layouts.layout')
@section('content')
    <table>
        <thead>
        <tr>
            <th>Day</th>
            <th>Month</th>
            <th>Year</th>
        </tr>
        </thead>
        <tbody>
        @foreach($dates as $date)
            <tr>
                <td>{{ $date->day }}</td>
                <td>{{ $date->month }}</td>
                <td>{{ $date->year }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <form action="/" method="get" class="search">
        <input class="input" type="number" name="id" placeholder="Enter date id" value="{{ $id }}">
        <button class="submit" type="submit">Search</button>
    </form>
@endsection