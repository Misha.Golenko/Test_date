<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Date extends Model
{
    protected $fillable = ['date'];

    private $dateTimeObject = null;

    public function getDayAttribute()
    {
        return $this->getDateTimeObject()->format('d');
    }

    public function getMonthAttribute()
    {
        return $this->getDateTimeObject()->format('m');
    }

    public function getYearAttribute()
    {
        return $this->getDateTimeObject()->format('Y');
    }

    /**
     * @return \DateTime|null
     */
    private function getDateTimeObject()
    {
        if (!$this->dateTimeObject) {
            $this->dateTimeObject = (new \DateTime($this->date));
        }

        return $this->dateTimeObject;
    }
}
