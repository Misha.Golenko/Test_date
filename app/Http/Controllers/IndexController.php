<?php

namespace App\Http\Controllers;

use App\Models\Date;
use App\Http\Requests\SearchRequest;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function index(SearchRequest $request)
    {

        $id = $request->id??"";

        if($id){
            $data = Date::where('id', $id);            
        }else{

            $data = new Date();
        }
        $dates = $data->get();

        return view('index', ['dates' => $dates,'id'=>$id]);
    }

}
