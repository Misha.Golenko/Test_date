<?php

namespace App\Providers;

use App\Actions\Action;
use App\Actions\GetDateAction;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(Action::class, function () {
            return GetDateAction::getInstance();
        });
    }
}
