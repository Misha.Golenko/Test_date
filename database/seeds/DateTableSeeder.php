<?php

use Illuminate\Database\Seeder;

class DateTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('dates')->truncate();

        $dates = [
            ['date' => '1992-11-23'],
            ['date' => '1991-01-18'],
            ['date' => '1988-07-31'],
            ['date' => '1976-04-21'],
            ['date' => '1943-12-17'],
            ['date' => '1998-10-14'],
            ['date' => '1954-09-09'],
            ['date' => '1912-01-03'],
            ['date' => '1997-02-11'],
        ];
        foreach ($dates as $value){
            \App\Models\Date::create($value);
        }

    }
}
